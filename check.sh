#!/bin/bash

###
# monitoring config.json for wallet config of apple pay
###

cd /opt/apple_pay/apple-pay/
wget -q https://smp-device-content.apple.com/static/region/v2/config.json -O /opt/apple_pay/apple-pay/config.json
curl --head https://smp-device-content.apple.com/static/region/v2/config.json > /opt/apple_pay/apple-pay/config.json.header
sed -i '/Cache-Control/d' /opt/apple_pay/apple-pay/config.json.header
sed -i '/Date/d' /opt/apple_pay/apple-pay/config.json.header
sed -i '/Accept-Ranges/d' /opt/apple_pay/apple-pay/config.json.header
sed -i '/Content-Length/d' /opt/apple_pay/apple-pay/config.json.header
sed -i '/TCP_MEM_HIT/d' /opt/apple_pay/apple-pay/config.json.header
sed -i '/TCP_REFRESH_MISS/d' /opt/apple_pay/apple-pay/config.json.header
git commit -a -m "autocommit"
git push origin master